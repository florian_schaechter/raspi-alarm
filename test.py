import time, datetime
import telepot
import sys
import re
from telepot.loop import MessageLoop
from pprint import pprint

with open('config','r') as f:
    api_key = f.read().rstrip()

bot = telepot.Bot(api_key)
pprint(bot.getMe())

now = datetime.datetime.now()

def handle(msg):
    #pprint(msg)

    chat_id = (msg['chat']['id'])
    command = str(msg['text'])

    #print(chat_id,command)
    #print(type(chat_id),type(command))

    if command=="/hi":
        #print("/hi")
        bot.sendMessage(chat_id,str("hello florian"))
    elif command=="/status":
        bot.sendMessage(chat_id,str("I'm fine, current time: ")+str(now))
    elif command.startswith("/set"):
        #print(command)
        s = re.split("[ _:]",command)
        if len(s) == 2:
            s.append("00")
        #print(len(s),s)
        if len(s) > 1:
            bot.sendMessage(chat_id,str("Okay, set alarm to: ")+s[1]+":"+s[2])
        else:
            bot.sendMessage(chat_id,str("Something was wrong with your command: "+command))
    else:
        bot.sendMessage(chat_id,str("Command not found: "+command))

MessageLoop(bot, handle).run_as_thread()
print ('Listening ...')

while 1:
    time.sleep(10)
