# Software
## Features:
Nope
## To-Do:
1. Activate LED-strip over x minutes to simulate sunrise
2. Do communication vial Telegram-Bot
    a. Select color
    b. Set next alarm
    c. Set music file
    d. Inform user in the evening about set alarms
3. Recurring Alarms +  delay for one event
4. Use online music player/web radio as audio source, backup file for no internet access
5. Message user if alarm played for more than x minutes
 
# Hardware
1. Let Raspberry control LED-Strip [here](https://dordnung.de/raspberrypi-ledstrip/)
2. Adding Speakers powered via USB *or* using smart plug (e.g. Osram) *or* Bluetooth speaker
    1.  Pro USB: Kill Power of USB ports is possible + cheaper + more reliable
    2.  Pro SmartPlug: more versatile  
    3.  Pro Bluetooth: accessible from other sources e.g. phone